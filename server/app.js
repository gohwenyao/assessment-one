/**
 * Server side app
 */

console.log("Starting server side app ...");

// Express, cors, body-parser, and file-system.
const express = require('express');
var cors = require('cors')
const bodyParser = require('body-parser');
const fs = require('fs');

// Instantiating express as "app".
var app = express();
//Incorporation of the bodyParser service into express for later interpretation of values.
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
// Incorporation of the middleware, cors, into the express side.
app.use(cors());
console.log(__dirname);
// Assign NODE_PORT value from OS environment variable.
const NODE_PORT = process.env.PORT;

// app.use(express.static(__dirname + "/../dist/"));

// Declare jsonData for manipulation.
var jsonData;
console.log(__dirname + '/data.json');
fs.readFile(__dirname + '/data.json', 'UTF-8', function(err, data){
  if(err){
    throw err;
  }
  jsonData = JSON.parse(data);
});

// get
// post
// put
// delete

// Creating the server endpoint.
app.post("/api/user/register", (req, res)=>{
  console.log(req);
  var user = req.body;
console.log(user);
//user.age = 40;
jsonData.push(user);


// Writing to jsonData.
fs.writeFile(__dirname + '/data.json', JSON.stringify(jsonData), 'utf8', function(data){
  console.log(data);
  res.status(200).json(jsonData);
});
});

function callback(result){
  console.log(result);
}

/*
app.get("/api/product/reviews", (req, res)=>{
   res.status(200).json(reviews);
});

app.get("/api/product/list", (req, res)=>{
   console.log(products);
   res.status(200).json(products);
});*/

// Incorporate Express JS listening codes into server/app.js
app.listen(NODE_PORT, function(){
  console.log(`Backend Server started at ${NODE_PORT}`);
})

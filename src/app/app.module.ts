import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {communication_service} from './services/communication.service';
import { SuccessComponent } from './success/success.component';


@NgModule({
  declarations: [
    AppComponent,
    SuccessComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    communication_service
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

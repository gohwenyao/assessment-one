import { Component, OnInit, Input } from '@angular/core';
import { UserData } from '../shared/user-model';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  @Input() users: UserData[];
  title: string = 'Thank you for your submission ';

  constructor() { }

  ngOnInit() {
  }

}

// Instantiate and declare UserData obj with its corresponding variables and types.
// This obj will be used to hold user input form data and maybe shared amongst different
// services and components.

export class UserData {
  constructor(
    public email: string,
    public password: string,
    public userName: string,
    public gender: string,
    public dob: Date,
    public address: string,
    public country: string,
    public contactNumber: string,
  ) {

  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs/Observable';

import { UserData } from '../shared/user-model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

  @Injectable()
  export class communication_service {
    private userAPIURL = `${environment.backendApiURL}/api/user/`

    constructor(private httpClient: HttpClient) { }

    public saveUserData(user: UserData) : Observable<UserData>{
      return this.httpClient.post<UserData>(this.userAPIURL + 'register', user, httpOptions);
    }

    }

import {Component, OnInit} from '@angular/core';
import { UserData } from './shared/user-model';
import { communication_service } from './services/communication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'Assessment One';
  userInfo = null;
  gender: string[] = ['M', 'F'];


  submission_flag: boolean = false;
  results: string = '';
  users: any;

  constructor(private user_communicationService: communication_service) {}

  // Initialize the component after Angular first displays the data-bound properties and sets the component's input properties.
  ngOnInit() {
    this.userInfo = new UserData('', '', '', '', null, '', '', '');
  }

  ok(){
    console.log(this.userInfo.email);
    console.log(this.userInfo.password);
    console.log(this.userInfo.userName);
    console.log(this.userInfo.gender);
    console.log(this.userInfo.dob);
    console.log(this.userInfo.address);
    console.log(this.userInfo.country);
    console.log(this.userInfo.contactNumber);
    this.user_communicationService.saveUserData(this.userInfo)
      .subscribe(users => {
        console.log('sending to backend!');
        console.log(users);
        this.users = users;
      });
    this.submission_flag = true;
  }

}
